<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Relations\Relation;

use Illuminate\Support\ServiceProvider;

use App\Product;
use App\Observers\ProductObserver;

use App\Recipe;
use App\Observers\RecipeObserver;

use App\Media;
use App\Observers\MediaObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::morphMap([
            'product' => 'App\Product',
        ]);

        Recipe::observe(RecipeObserver::class);
        Media::observe(MediaObserver::class);
        Product::observe(ProductObserver::class);
    }
}
