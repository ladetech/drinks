<?php

namespace App;

class Cart
{
    private $items;

    public function __construct($items)
    {
        $this->items = $items;
    }

    public function getItems()
    {
        return array_map(function ($item) {
            if ($item['type'] == 'product') {
                return [
                    'item' => Product::where('id', $item['id'])->first(),
                    'quantity' => $item['quantity']
                ];
            }
            return [
                'item' => ['id' => $item['id']],
                'quantity' => $item['quantity']
            ];
        }, $this->items);
    }

    public function setItems()
    {
        $this->items = $items;
    }

    public function getLineItems()
    {
        return array_map(function ($item) {
            return [
                'name' => $item['item']->name,
                'description' => $item['item']->type,
                'images' => [$item['item']->fullImageUrl],
                'amount' => $item['item']->price * 100,
                'currency' => 'mxn',
                'quantity' => $item['quantity'],
            ];
        }, $this->getItems());
    }

    public function addItem($item)
    {
        array_push($this->items, $item);
    }
}
