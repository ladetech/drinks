<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token', 'role', 'created_at', 'updated_at'
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'phone_numbers' => 'array',
    ];

    public function addresses()
    {
        return $this->hasMany('App\Address');
    }

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    public function pendingPaymentOrders()
    {
        return $this->hasMany('App\Order')->where('paid', 0);
    }

    public function paidOrders()
    {
        return $this->hasMany('App\Order')->where('paid', 1);
    }

    public function pendingShipmentOrders()
    {
        return $this->hasMany('App\Order')->where('shipped', 0);
    }

    public function linkedSocialAccounts()
    {
        return $this->hasMany('App\LinkedSocialAccount');
    }
}
