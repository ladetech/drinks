<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Illuminate\Support\Facades\DB;

use App\Product;
use App\Media;

class DeploymentController extends Controller
{
    public function postDeployment()
    {
        \Artisan::call('clear-compiled');
        \Artisan::call('migrate --force');
        \Artisan::call('config:cache');
        \Artisan::call('route:cache');
        \Artisan::call('view:cache');
    }
}
