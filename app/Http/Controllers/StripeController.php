<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Stripe\Event as StripeEvent;
use App\Order;

class StripeController extends Controller
{
    public function handleWebhook(Request $request)
    {
        $event = StripeEvent::constructFrom($request->all());

        switch ($event->type) {
        case 'checkout.session.completed':
            $order = Order::where('stripe_session_id', $event->data->object->id)->first();
            if (!$order)
                return response('', 404);
            $order->paid = true;
            $order->save();
            break;
        default:
            return response('', 400);
            break;
        }
        return response('', 200);
    }
}
