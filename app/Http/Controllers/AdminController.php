<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function showDashboard()
    {
        return view('admin.dashboard');
    }

    public function showMediaDashboard()
    {
        return view('admin.media_dashboard');
    }

    public function showOfferDashboard()
    {
        return view('admin.offer_dashboard');
    }

    public function showOrderDashboard()
    {
        return view('admin.order_dashboard');
    }

    public function showProductDashboard()
    {
        return view('admin.product_dashboard');
    }

    public function showRecipeDashboard()
    {
        return view('admin.recipe_dashboard');
    }

    public function showStoreDashboard()
    {
        return view('admin.store_dashboard');
    }
}
