<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function showDashboard()
    {
        $user = \Auth::user();
        return view('user.dashboard', ['user' => $user]);
    }

    public function showAddresses()
    {
        return view('user.address_dashboard');
    }

    public function showOrders()
    {
        // delete expired orders
        \App\Order::expired()->delete();
        return view('user.order_dashboard');
    }

    public function showConfig()
    {
        return view('user.configuration', ['user' => \Auth::user()]);
    }

    public function updateInfo(Request $request)
    {
        $user = \Auth::user();
        $user->name = $request->name;
        $user->email = $request->email;

        if ($request->phone_numbers)
            $user->phone_numbers = json_decode($request->phone_numbers, TRUE);

        $user->save();
    }

    public function changePassword(Request $request)
    {
        $user = \Auth::user();

        if (!Hash::check($user->password, $request->old_password))
            return response('Contraseña incorrecta', 400);

        $user->password = Hash::make($request->new_password);
        $user->save();
    }

    public function deleteAccount(Request $request)
    {
        if (!Hash::check($user->password, $request->password))
            return response('Contraseña incorrecta', 400);

        $user = \Auth::user();
        $user->delete();

        \Auth::logout();
    }

    public function logout()
    {
        \Auth::logout();
    }

    public function getPhoneNumbers(Request $request)
    {
        return response()->json($request->user()->phone_numbers);
    }

    public function updatePhoneNumbers(Request $request)
    {
        $user = $request->user();
        $user->phone_numbers = json_decode($request->phone_numbers, TRUE);
        $user->save();

        return response()->json($user->phone_numbers);
    }
}
