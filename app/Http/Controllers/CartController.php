<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Product;
use App\Cart;

class CartController extends Controller
{
    public function __construct(Request $request)
    {
        if (!$request->session()->has('cart'))
            $request->session()->put('cart', []);
    }

    public function index()
    {
        return view('cart');
    }

    public function show(Request $request)
    {
        $cart = new Cart($request->session()->get('cart', []));
        return $cart->getItems();
    }

    public function add(Request $request)
    {
        $cartItem = [
            'type' => '',
            'id' => NULL,
            'quantity' => $request->quantity
        ];

        if (!$request->has('type'))
            return response(400);

        Product::where('id', $request->id)->firstOrFail();

        $cartItem['type'] = $request->type;
        $cartItem['id'] = $request->id;

        try {
            $request->session()->push('cart', $cartItem);
            return response()->json(['success' => true]);
        } catch (Exception $e) {
            return response(500);
        }
    }

    public function update(Request $request)
    {
        $items = $request->session()->pull('cart', []);

        $cartItem = [
            'type' => $request->type,
            'id' => $request->id,
            'quantity' => $request->quantity
        ];

        $i = 0;

        foreach ($items as $item) {
            if ($item['id'] == $cartItem['id']) {
                $items[$i]['quantity'] = $cartItem['quantity'];
                break;
            }
            $i++;
        }

        $request->session()->put('cart', $items);
        return response()->json(['success' => true]);
    }

    public function remove(Request $request)
    {
        $items = $request->session()->pull('cart', []);

        $newItems = array_filter($items, function ($item) {
            global $request;
            return $item['id'] != $request->id;
        });

        $request->session()->put('cart', $newItems);
        return response()->json(['success' => true]);
    }
}
