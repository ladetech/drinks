<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;
use App\Category;

class ProductController extends Controller
{
    private $indexParams = ['id', 'name', 'slug', 'category_id', 'price', 'image_path', 'volume', 'volume_units'];

    public function showAll()
    {
        return view('product.all');
    }

    public function show($slug)
    {
        return Product::where('slug', $slug)->firstOrFail();
    }

    public function getRelated($id, Request $request)
    {
        $limit = 4;

        if ($request->has('limit'))
            $limit = $request->limit;

        $queryParams = $this->indexParams;

        if ($request->has('params'))
            $queryParams = array_unique(array_merge($queryParams, $request->params));

        $relatedProductIds = Product::findOrFail($id)->relatedProducts($queryParams)->take($limit)->pluck('id');

        return Product::select($queryParams)->find($relatedProductIds);
    }

    public function getRecipes($id)
    {
        return Product::findOrFail($id)->recipes;
    }

    public function index(Request $request)
    {
        $queryParams = $this->indexParams;
        // only info needed
        if ($request->has('params'))
            $queryParams = array_unique(array_merge($queryParams, $request->params));
        return Product::get($queryParams);
    }

    public function indexHighlighted(Request $request)
    {
        $queryParams = $this->indexParams;
        // only info needed
        if ($request->has('params'))
            $queryParams = array_unique(array_merge($queryParams, $request->params));
        return Product::highlighted()->get($queryParams);
    }

    public function showInfo(string $slug)
    {
        $product = \App\Product::where('slug', $slug)->firstOrFail();

        $product->visits()->increment();

        return view('product.show', ['product' => $product]);
    }

    public function insert(Request $request)
    {
        $newProduct = new Product;

        $newProduct->copyFromRequest($request);

        $newProduct->save();

        if ($request->hasFile('image') && $request->file('image')->isValid())
            $newProduct->image = $request->file('image');

        return $newProduct;
    }

    public function edit(Request $request)
    {
        $product = Product::findOrFail($request->id);

        $product->copyFromRequest($request);

        if ($request->hasFile('image') && $request->file('image')->isValid())
            $product->image = $request->file('image');
        else
            \Log::info("NO HAY IMAGEN AHI QUE SHOW");

        $product->save();

        return $product;
    }

    public function search(Request $request)
    {
        // set defaults
        $order = $request->order ?: 'id,desc';

        $search = [
            'query' => trim($request->get('query')) ?: '',
            'orderColumn' => explode(',', $order)[0],
            'orderAscDesc' => explode(',', $order)[1],
            'resultsPerPage' => $request->rpp ?: 12, // perfect for columns
            'page' => $request->page ?: 1 // first page
        ];

        $category_ids = [];

        if ($request->has('category_id')) {
            array_push($category_ids, $request->category_id);

            $descendants = Category::findOrFail($request->category_id)->descendantIds;

            foreach ($descendants as $did) {
                array_push($category_ids, $did);
            }
        }


        if ($search['query']) {
            $query = Product::search($search['query'])->query(function ($builder) {
                global $request;

                $builder->with('category');

                if (count($category_ids) > 0) {
                    $builder->whereIn('category_id', $category_ids);
                }
            });
        } else {
            $query = Product::query();
            if (count($category_ids) > 0) {
                $query->whereIn('category_id', $category_ids);
            }
        }

        $query->orderBy($search['orderColumn'], $search['orderAscDesc']);

        return $query->get();
    }

    public function delete($id, Request $request)
    {
        $product = Product::findOrFail($id);

        $product->delete();
    }
}
