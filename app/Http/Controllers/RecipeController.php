<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Recipe;

class RecipeController extends Controller
{
    public function index()
    {
        return Recipe::get();
    }

    public function insert(Request $request)
    {
        $recipe = Recipe::fromRequest($request);
        $recipe->save();
        $recipe->products = explode(',', $request->products);
        $recipe->save();
        return response()->json([
            'recipe' => $recipe,
        ]);
    }

    public function edit(Request $request)
    {
        $recipe = Recipe::fromRequest($request);
        $recipe->save();
        return response()->json([
            'recipe' => $recipe,
        ]);
    }

    public function delete(Request $request)
    {
        $recipe = Recipe::where('slug', $request->slug)->first();
        $recipe->delete();
    }

    public function showRecipe($slug)
    {
        $recipe = Recipe::where('slug', $slug)->first();
        return view('recipe.show', ['recipe' => $recipe]);
    }
}
