<?php

namespace App\Http\Controllers;

use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

use App\Product;
use App\Category;
use App\Order;

class StatsController extends Controller
{
    public function summary(Request $request)
    {
        $summary = [
            'most_sold_product' => null,
            'most_sold_category' => null,
            'category_sales' => [],
            'product_sales' => [],
            'sales_count' => 0
        ];

        /* establish interval for the summary */
        if ($request->has('interval') && $request->interval != 'all_time') {
            $interval = $request->interval;
            $from = Carbon::now()->subtract(1, $interval);
        } else {
            $interval = 'all_time';
            $from = 0;
        }

        // fetch products + total sales quantity
        $top20Products = DB::table('products')
            ->select(
                'products.id',
                'products.name',
                'products.highlighted',
                'products.category_id'
            )
            ->join('order_product', 'order_product.product_id', 'products.id')
            ->join('orders', function ($join) use ($from) {
                $join->on('orders.id', 'order_product.order_id')
                    ->where('orders.updated_at', '>', $from)
                    ->where('orders.paid', 1)
                    ->where('orders.delivered', 1);
            })
            ->groupBy('order_product.product_id')
            ->addSelect(
                DB::raw('SUM(order_product.quantity) as sold')
            )
            ->orderBy('sold', 'DESC')
            ->take(15)->get()->map(function ($productSale) {
                if ($productSale->category_id) {
                    $productSale->type = Category::find($productSale->category_id)
                                ->fullName;
                }
                return $productSale;
            });
        // end fetching products
        $summary['product_sales'] = $top20Products;
        $summary['most_sold_product'] = $top20Products->first();

        // fetch categories + total sales
        $top20Categories = DB::table('categories')
            ->select('categories.id')
            ->join('products', 'products.category_id', '=', 'categories.id')
            ->join('order_product', 'order_product.product_id', '=', 'products.id')
            ->join('orders', function ($join) use ($from) {
                $join->on('orders.id', 'order_product.order_id')
                    ->where('orders.updated_at', '>', $from)
                    ->where('orders.paid', 1)
                    ->where('orders.delivered', 1);
            })
            ->groupBy('products.category_id')
            ->addSelect(
                DB::raw('SUM(order_product.quantity) as sold')
            )
            ->orderBy('sold', 'DESC')
            ->take(20)->get()->map(function ($categorySale) {
                if ($categorySale->id) {
                    $category = Category::find($categorySale->id);
                    $categorySale->name = $category->name;
                    $categorySale->full_name = $category->full_name;
                }
                return $categorySale;
            });
        $summary['category_sales'] = $top20Categories;
        $summary['most_sold_category'] = $top20Categories->first();
        // end fetching categories
        //
        $summary['sales_count'] = Order::successful()->where('updated_at', '>', $from)->count();

        return response()->json($summary);
    }
}
