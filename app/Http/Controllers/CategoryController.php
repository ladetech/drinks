<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Category;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        // all root categories
        // avoids repeating subcategories
        return Category::root()->with('subcategories')->get();
    }

    public function indexRoot()
    {
        return Category::root()->get(['slug', 'name', 'id']);
    }

    public function products($categoryId)
    {
        return Category::findOrFail($categoryId)->products;
    }

    /**
     * Creates or edits a category
     */
    public function save(Request $request)
    {
        $category = $request->has('id') ? Category::findOrFail($request->id) : new Category();
        $category->name = $request->name;
        $category->parent_id = $request->parent_id == 'null' ? null : $request->parent_id;

        if ($category->isDirty())
            $category->save();

        return response()->json($category);
    }

    public function delete($id)
    {
        Category::findOrFail($id)->delete();
    }
}
