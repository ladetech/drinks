<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\LinkedSocialAccount;
use Socialite;

class FacebookController extends Controller
{
    public function redirect()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleCallback()
    {
        $user = Socialite::driver('facebook')->user();
        $linkedSocialAccount = LinkedSocialAccount::where('provider_name', 'facebook')->where('provider_id', $user->getId())->first();

        if (!$linkedSocialAccount) {
            $u = new \App\User();
            $u->name = $user->getName();

            if ($user->getEmail()) {
                $u->email = $user->getEmail();
                $u->email_verified_at = now();
            }

            $linkedSocialAccount = new LinkedSocialAccount();
            $linkedSocialAccount->provider_name = 'facebook';
            $linkedSocialAccount->provider_id = $user->getId();
            $u->save();
            $linkedSocialAccount->user_id = $u->id;
            $linkedSocialAccount->save();
        } else {
            $u = $linkedSocialAccount->user;
        }

        if (\Auth::loginUsingId($user->id))
            return redirect()->route('home');
        else
            return redirect()->route('login');
    }
}
