<?php

namespace App\Http\Controllers;

use App\Store;

use Illuminate\Http\Request;

class StoreController extends Controller
{
    public function index()
    {
        return Store::all();
    }

    public function get($id)
    {
        return Store::findOrFail($id);
    }

    public function insert(Request $request)
    {
        $store = Store::fromRequest($request);
        $store->save();
        return response()->json(['store' => $store]);
    }

    public function edit(Request $request)
    {
        $store = Store::fromRequest($request);
        $store->save();
        return response()->json(['store' => $store]);
    }

    public function delete(Request $request)
    {
        $store = Store::findOrFail($request->id);
        $store->delete();
        return response()->json(['deleted' => $request->id]);
    }
}
