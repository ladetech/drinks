<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Media;

class MediaController extends Controller
{
    public function list(Request $request)
    {
        $media = Media::query();

        if ($request->has('role')) {
            $media = $media->where('role', $request->role);
        }

        return $media->get();
    }

    public function create(Request $request)
    {
        if (!$request->hasFile('files'))
            return response('[]', 406);

        $uploadedFiles = [];

        foreach ($request->file('files') as $file) {
            $media = new Media();
            $media->mediable_type = $request->mediable_type;
            $media->mediable_id = $request->mediable_id;
            $media->description = $request->description;

            if ($request->has('role')) {
                $media->role = $request->role;
            }

            if ($media->file = $file) {
                $media->save();
                array_push($uploadedFiles, $media);
            }
        }

        return response()->json($uploadedFiles, 201);
    }

    public function update($id, Request $request)
    {
        $medium = Media::findOrFail($id);

        if ($request->has('description')) {
            $medium->description = $request->description;
        }

        if ($medium->isDirty())
            $medium->save();
    }

    public function delete($id)
    {
        Media::findOrFail($id)->delete();
        return response('', 200);
    }
}
