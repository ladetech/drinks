<?php

namespace App\Http\Controllers;

use App\Address;

use Illuminate\Http\Request;

class AddressController extends Controller
{
    public function index(Request $request)
    {
        return $request->user()->addresses;
    }

    public function add(Request $request)
    {
        $address = Address::fromRequest($request);
        $request->user()->addresses()->save($address);
        $address->save();
        return response()->json($address);
    }

    public function edit(Request $request)
    {
        $address = Address::fromRequest($request);
        $request->user()->addresses()->save($address);
        $address->save();
        return response()->json($address);
    }

    public function remove($id)
    {
        Address::findOrFail($id)->delete();
    }
}
