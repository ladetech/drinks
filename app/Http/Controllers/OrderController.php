<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Order;

class OrderController extends Controller
{
    public function indexAdminPending(Request $request)
    {
        $this->middleware('admin');
        return Order::adminPending()->with(['products:id,name,price,order_product.quantity,category_id', 'address:id,address,city,state,country,zip_code,extra_info', 'user:id,name,phone_numbers'])->get();
    }

    public function markAsShipped($id)
    {
        $order = Order::findOrFail($id);
        $order->shipment_status = "Enviado";
        $order->save();
    }

    public function updateShipmentStatus($id, Request $request)
    {
        $order = Order::findOrFail($id);
        $order->shipment_status = $request->shipment_status;
        $order->save();
    }

    public function markAsDelivered($id)
    {
        $order = Order::findOrFail($id);
        $order->shipment_status = "Entregado";
        $order->delivered = true;
        $order->save();
    }

    public function index(Request $request)
    {
        $userOrders = \Auth::user()->orders;

        if ($request->append) {
            foreach ($request->append as $append) {
                switch ($append) {
                case 'products.quantity':
                    $userOrders = $userOrders->each->append('products_quantity');
                    break;
                }
            }
        }

        return $userOrders;
    }

    public function prepareCheckout()
    {
        return view('order.prepare_checkout');
    }

    public function create(Request $request)
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        $cart = new \App\Cart($request->session()->get('cart', []));
        $order = \App\Order::fromRequest($request);

        $session = \Stripe\Checkout\Session::create([
            'customer_email' => \Auth::user()->email,
            'payment_method_types' => ['card'],
            'line_items' => [$cart->getLineItems()],
            'success_url' => route('user.orders'),
            'cancel_url' => route('cart.show')
        ]);

        $order->stripe_session_id = $session->id;
        $order->save();

        // empty shopping cart on successful order
        $request->session()->put('cart', []);

        return response()->json(['order_id' => $order->id]);
    }

    public function redirectToCheckout(Request $request)
    {
        $order = Order::findOrFail($request->order_id);

        if ($order->paid)
            return redirect()->route('order.showStatus', ['order_id' => $request->order_id]);

        return view('order.redirect_to_checkout', ['order' => $order]);
    }

    public function showStatus(Request $request)
    {
        $order = Order::with('products:id,name,price,order_product.quantity,category_id')->findOrFail($request->order_id);
        return view('order.show_status', ['order' => $order]);
    }

    public function success(Request $request)
    {
        $order = Order::where('stripe_session_id', $request->session_id)->first();
        return view('order.show_status', ['order' => $order]);
    }
}
