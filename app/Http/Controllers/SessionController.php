<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SessionController extends Controller
{
    public function confirmAge()
    {
        session(['is-over-18' => true]);
        $response = new Response();
        $response->withCookie(cookie()->forever('is-over-18', 'true'));
        return $response;
    }

    public function acceptCookies()
    {
        session(['accepted-cookies' => true]);
        $response = new Response();
        $response->withCookie(cookie()->forever('accepted-cookies', 'true'));
        return $response;
    }
}
