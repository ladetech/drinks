<?php

namespace App\Listeners;

use App\Events\ProductEdited;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Http\Controllers\ProductRelationsController;

class UpdateProductRelations
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProductEdited  $event
     * @return void
     */
    public function handle(ProductEdited $event)
    {
        ProductRelationsController::update($event->product);
    }
}
