<?php

namespace App\Listeners;

use App\Events\ProductStored;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Http\Controllers\ProductRelationsController;

class CreateProductRelations
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProductStored  $event
     * @return void
     */
    public function handle(ProductStored $event)
    {
        ProductRelationsController::create($event->product);
    }
}
