<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $timestamps = false;

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function subcategories()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id')->with('subcategories');
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;

        if ($this->isDirty('name'))
            $this->slug = \Str::slug($value) . '-' . \Str::random(2);
    }

    /**
     * Gets this category's products, including subcategories'
     */
    public function getAllProductsAttribute()
    {
        $products = $this->products;

        foreach ($this->subcategories as $subcategory)
            $products->push($subcategory->allProducts);

        return $products;
    }

    public function getAllProductsCountAttribute()
    {
        $products = $this->products()->count();

        foreach ($this->subcategories as $subcategory)
            $products += $subcategory->allProductsCount;

        return $products;
    }

    public function scopeRoot($query)
    {
        return $query->whereNull('parent_id');
    }

    public function getFullNameAttribute()
    {
        if ($this->attributes['parent_id'])
            return $this->parent->fullName . ' ' . $this->name;
        return $this->name;
    }

    public function getOrdersAttribute()
    {
        $query = Order::join('order_product', 'orders.id', '=', 'order_product.order_id')
            ->join('products', 'products.id', '=', 'order_product.product_id')
            ->where('products.category_id', $this->id);
        $descendantIds = $this->descendantIds;
        foreach ($descendantIds as $id) {
            $query->orWhere('products.category_id', $id);
        }
        return $query;
    }

    public function getOrdersCountAttribute()
    {
        return $this->orders->count();
    }

    public function getAncestorIdsAttribute()
    {
        if ($this->parent_id === null)
            return [];

        $ancestorIds = $this->parent->ancestorIds;
        array_push($ancestorIds, $this->parent_id);

        return $ancestorIds;
    }

    public function getDescendantIdsAttribute()
    {
        $descendants = $this->subcategories;
        $descendantIds = $descendants->pluck('id')->toArray();

        foreach ($descendants as $descendant)
            $descendantIds = array_merge($descendantIds, $descendant->descendantIds);

        return $descendantIds;
    }
}
