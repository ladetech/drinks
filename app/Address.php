<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Address extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    static function fromRequest(Request $request)
    {
        $address = isset($request->id) ? Address::findOrFail($request->id) : new Address();
        $address->name = $request->name;
        $address->zip_code = $request->zip_code;
        $address->address = $request->address;
        $address->country = $request->country;
        $address->suburb = $request->suburb;
        $address->state = $request->state;
        $address->city = $request->city;
        $address->extra_info = $request->extra_info;
        return $address;
    }
}
