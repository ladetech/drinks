<?php

namespace App\Observers;

use App\Recipe;

class RecipeObserver
{
    /**
     * Handle the recipe "created" event.
     *
     * @param  \App\Recipe  $recipe
     * @return void
     */
    public function created(Recipe $recipe)
    {
        \Log::channel('single')->debug("Created recipe: " . $recipe->name);
    }

    /**
     * Handle the recipe "updated" event.
     *
     * @param  \App\Recipe  $recipe
     * @return void
     */
    public function updated(Recipe $recipe)
    {
        \Log::channel('single')->debug("Updates recipe: " . $recipe->name);
    }

    /**
     * Handle the recipe "deleted" event.
     *
     * @param  \App\Recipe  $recipe
     * @return void
     */
    public function deleted(Recipe $recipe)
    {
        if (\Storage::disk('public')->exists('recipes/' . $recipe->image_path))
            \Log::channel('single')->debug('Image ' . $recipe->image_path . 'exists.');
        else
            \Log::channel('single')->debug('Image ' . $recipe->image_path . 'does not exist.');

        if (\Storage::disk('public')->exists('recipes/' . $recipe->image_path))
            \Storage::disk('public')->delete('recipes/' . $recipe->image_path);
        if (\Storage::disk('public')->exists('recipes/' . $recipe->image_path))
            \Log::channel('single')->error('Did not delete recipe image for recipe ' . $recipe->slug . '.');
    }

    /**
     * Handle the recipe "restored" event.
     *
     * @param  \App\Recipe  $recipe
     * @return void
     */
    public function restored(Recipe $recipe)
    {
        //
    }

    /**
     * Handle the recipe "force deleted" event.
     *
     * @param  \App\Recipe  $recipe
     * @return void
     */
    public function forceDeleted(Recipe $recipe)
    {
        //
    }
}
