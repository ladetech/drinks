<?php

namespace App\Observers;

use App\Product;
use App\ProductRelation;

class ProductObserver
{
    /**
     * Handle the product "created" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function created(Product $product)
    {
        $allOtherProducts = Product::where('id', '!=', $product->id)->get();

        foreach ($allOtherProducts as $otherProduct) {
            $relation = new ProductRelation;
            $relation->product_a = $product->id;
            $relation->product_b = $otherProduct->id;
            $relation->correlation = $product->calculateCorrelation($otherProduct);
            $relation->save();
        }
    }

    /**
     * Handle the product "updated" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function updated(Product $product)
    {
        $allOtherProducts = Product::where('id', '!=', $product->id)->get();

        foreach ($allOtherProducts as $otherProduct) {
            $relationIndex = $product->calculateCorrelation($otherProduct);

            $relation = ProductRelation::where(function ($query) use ($product, $otherProduct) {
                $query->where('product_a', $product->id)->where('product_b', $otherProduct->id);
            })->orWhere(function ($query) use ($product, $otherProduct) {
                $query->where('product_b', $product->id)->where('product_a', $otherProduct->id);
            })->first();

            $relation->correlation = $product->calculateCorrelation($otherProduct);
            $relation->save();
        }
    }

    /**
     * Handle the product "deleted" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function deleted(Product $product)
    {
        \DB::table('product_relations')->where('product_a', $product->id)->orWhere('product_b', $product->id)->delete();

        if ($img = $product->image) {
            $img->delete();
        }
    }

    /**
     * Handle the product "restored" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function restored(Product $product)
    {
        //
    }

    /**
     * Handle the product "force deleted" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function forceDeleted(Product $product)
    {
        //
    }
}
