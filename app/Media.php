<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class Media extends Model
{
    public $timestamps = false;

    public static $MEDIA_PATH = 'public/media';
    public static $THUMBNAIL_PATH = 'public/media/thumbs';

    protected $appends = ['thumbnail_url', 'url'];
    protected $hidden = ['file_basename'];

    public function mediable()
    {
        return $this->morphTo();
    }

    public function setFileAttribute($file)
    {
        $path = Storage::putFile(Media::$MEDIA_PATH, $file);

        $this->attributes['type'] = explode('/', $file->getMimeType())[0];

        try {
            if (isset($this->attributes['file_basename']))
                // delete file if it exists
                $this->deleteFile();

            $basename = basename($path);
            $this->attributes['file_basename'] = $basename;

            if ($this->attributes['type'] == "image") {
                // delete thumbnail if it exists
                if (Storage::exists(Media::$THUMBNAIL_PATH . "/" . $basename))
                    Storage::delete(Storage::exists(Media::$THUMBNAIL_PATH . "/" . $basename));

                $thumbnail = Image::make($file);
                $thumbnail->resize(null, 300, function ($constraint) {
                    $constraint->aspectRatio();
                });

                $thumbnailPath = Storage::put(Media::$THUMBNAIL_PATH . "/" . $basename, $thumbnail->encode());
            }

            return true;
        } catch (\Exception $e) {
            \Log::error("Exception thrown storing media: " . $e->getMessage());

            Storage::delete($path);
            \Log::debug($path . " deleted.");

            if (isset($thumbnailPath)) {
                Storage::delete($thumbnailPath);
                \Log::debug($thumbnailPath . " deleted.");
            }

            return false;
        }
    }

    public function getUrlAttribute()
    {
        return Storage::url(Media::$MEDIA_PATH . "/" . $this->attributes['file_basename']);
    }

    public function getThumbnailUrlAttribute()
    {
        $thumbnailPath = Media::$THUMBNAIL_PATH . "/" . $this->attributes['file_basename'];
        if (Storage::exists($thumbnailPath))
            return Storage::url($thumbnailPath);
        return NULL;
    }

    public function scopeImage($query)
    {
        return $query->where('type', 'image');
    }

    public function scopeDocument($query)
    {
        return $query->where('type', 'application');
    }

    public function scopeVideo($query)
    {
        return $query->where('type', 'video');
    }

    public function deleteFile()
    {
        Storage::delete(Media::$MEDIA_PATH . "/" . $this->attributes['file_basename']);
    }

    public function deleteThumbnail()
    {
        Storage::delete(Media::$THUMBNAIL_PATH . "/" . $this->attributes['file_basename']);
    }
}
