<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Carbon\Carbon;

class Order extends Model
{
    protected $fillable = ['amount','paid', 'delivered', 'conflict', 'shipment_status',];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    public function __construct()
    {
        $this->id = \Str::uuid()->toString();
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function address()
    {
        return $this->belongsTo('App\Address');
    }

    public function products()
    {
        return $this->belongsToMany('App\Product');
    }

    public function getProductsQuantityAttribute()
    {
        return $this->products()->withPivot(['quantity'])->sum('quantity');
    }

    public function setProductsAttribute($items)
    {
        $this->amount = 0;
        $cart = new Cart($items);
        foreach ($cart->getItems() as $item) {
            $this->products()->save($item['item'], ['quantity' => $item['quantity']]);
            $this->amount += $item['item']->price * $item['quantity'];
        }
    }

    static function fromRequest(Request $request)
    {
        $order = new Order();
        $order->user_id = $request->user()->id;
        $order->address_id = $request->address_id;
        $order->stripe_session_id = $request->stripe_session_id;
        $order->amount = 0;
        $order->save();
        $order->products = $request->session()->get('cart', []);
        return $order;
    }

    static function scopeExpired()
    {
        return Order::where('paid', 0)->where('created_at', '<', Carbon::now()->subHours(3));
    }

    static function scopeAdminPending() {
        return Order::where('paid', 1)->where('delivered', 0)->orWhere('conflict', 1);
    }

    static function scopeSuccessful()
    {
        return Order::where('paid', 1)->where('delivered', 1);
    }
}
