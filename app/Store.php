<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Spatie\Geocoder\Geocoder;

class Store extends Model
{
    public $timestamps = false;

    private function updateCoordinates()
    {
        $client = new \GuzzleHttp\Client();
        $geocoder = new Geocoder($client);
        $geocoder->setApiKey(config('geocoder.key'));
        $result = $geocoder->getCoordinatesForAddress($this->attributes['address']);
        if ($result) {
            $this->attributes['lat'] = $result['lat'];
            $this->attributes['lng'] = $result['lng'];
        }
    }

    public function setAddressAttribute($address)
    {
        $this->attributes['address'] = $address;
        $this->updateCoordinates();
    }

    static function fromRequest(Request $request)
    {
        if ($request->id)
            $store = Store::find($request->id);
        else
            $store = new Store();

        $store->name = $request->name;
        $store->address = $request->address;

        return $store;
    }
}
