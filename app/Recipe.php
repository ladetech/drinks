<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Http\Request;

class Recipe extends Model
{
    protected $fillable = ['name', 'image_path', 'description', 'ingredients', 'slug', 'steps'];

    protected $casts = [
        'ingredients' => 'array'
    ];

    protected $appends = ['image_url', 'ingredients', 'products'];

    public $timestamps = false;

    private function updateSlug()
    {
        $this->attributes['slug'] = \Str::slug($this->name);
    }

    static function fromRequest(Request $request)
    {
        if ($request->has('slug') && $request->slug)
            $recipe = Recipe::where('slug', $request->slug)->first();
        else
            $recipe = new Recipe();

        $recipe->name = $request->name;
        $recipe->description = $request->description;
        if ($request->image)
            $recipe->image = $request->image;
        $recipe->ingredients = explode(',', $request->ingredients);

        if ($request->slug) {
            $productSlugs = explode(',', $request->products);
            $recipe->products = $productSlugs;
        }

        $recipe->steps = $request->steps;

        return $recipe;
    }

    public function setNameAttribute(string $value)
    {
        $this->attributes['name'] = $value;
        $this->updateSlug();
    }

    public function setImageAttribute(UploadedFile $file)
    {
        if ($path = $file->store('recipes', 'public')) {
            if ($this->image_path && \Storage::disk('public')->exists('recipes/' . $this->image_path))
                \Storage::disk('public')->delete('recipes/' . $this->image_path);
            // exclude directory name
            $this->attributes['image_path'] = explode('/', $path)[1];
        }
    }

    public function getIngredientsAttribute()
    {
        return json_decode($this->attributes['ingredients']);
    }

    public function getImageUrlAttribute()
    {
        return '/storage/recipes/' . $this->image_path;
    }

    public function products()
    {
        return $this->belongsToMany('App\Product');
    }

    public function setProductsAttribute($productSlugs)
    {
        $currentProducts = [];

        foreach ($this->products as $product) {
            if (!in_array($product->slug, $productSlugs))
                $this->products()->detach($product);
            else
                array_push($currentProducts, $product->slug);
        }

        foreach ($productSlugs as $slug)
            if (!in_array($slug, $currentProducts))
            $this->products()->attach(\App\Product::where('slug', $slug)->first());
    }

    public function getProductsAttribute()
    {
        return $this->products()->get();
    }
}
