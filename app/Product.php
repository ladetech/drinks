<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Laravel\Scout\Searchable;

class Product extends Model
{
    use Searchable;

    protected $fillable = ['slug', 'name', 'abv', 'description', 'flavor', 'volume', 'volume_units', 'pairing', 'ideal_for', 'price', 'highlighted'];

    protected $casts = [
        'flavor' => 'array',
        'pairing' => 'array',
        'ideal_for' => 'array',
    ];

    protected $hidden = ['image_path'];

    protected $appends = ['image_url', 'thumbnail_url', 'type'];

    public $timestamps = false;

    public function media()
    {
        return $this->morphMany(Media::class, 'mediable');
    }

    public function thumbnail()
    {
        return $this->morphOne(Media::class, 'mediable')->image();
    }

    public function toSearchableArray()
    {
        $array = [
            'id' => $this->id,
            'name' => $this->name,
        ];

        if ($this->category)
            $array['type'] = $this->type;

        return $array;
    }

    public function recipes()
    {
        return $this->belongsToMany('App\Recipe');
    }

    public function visits()
    {
        return visits($this);
    }

    public function orders()
    {
        return $this->belongsToMany('App\Order');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function getTypeAttribute()
    {
        if ($this->category_id)
            return $this->category->fullName;
        return 'Otro';
    }

    private function updateSlug()
    {
        $this->slug = \Str::slug($this->name . "-" . $this->volume . $this->volume_units);
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;
        $this->updateSlug();
    }

    public function getImageAttribute()
    {
        return $this->media()->where('role', '=', 'pr')->first();
    }

    public function getThumbnailUrlAttribute()
    {
        if ($img = $this->image)
            return $img->thumbnail_url;
    }

    public function getImageUrlAttribute()
    {
        $img = $this->image;
        if ($img)
            return $img->url;
        return NULL;
    }

    public function setVolumeAttribute($value)
    {
        $this->attributes['volume'] = $value;
        $this->updateSlug();
    }

    public function setVolumeUnitsAttribute($value)
    {
        $this->attributes['volume_units'] = $value;
        $this->updateSlug();
    }

    public function setImageAttribute(UploadedFile $file)
    {
        if ($this->image)
            $this->image->delete();
        $medium = new Media();
        $medium->role = 'pr';
        $medium->mediable_type = 'product';
        $medium->mediable_id = $this->id;
        $medium->file = $file;
        $medium->save();
    }

    public function setHighlightedAttribute($val)
    {
        switch (gettype($val)) {
        case 'string':
            $this->attributes['highlighted'] = $val == 'true';
            break;
        case 'boolean':
            $this->attributes['highlighted'] = $val;
            break;
        case 'integer':
            $this->attributes['highlighted'] = $val != 0;
            break;
        }
    }

    /**
     * calculateCorrelation
     * @param App\Product $that
     *
     * @returns float $relationIndex
     *
     * returns the weighed relation between two product's attributes
     */
    public function calculateCorrelation(Product $that) : float
    {
        $relationIndex = 0;
        // TODO: Add correlation from categories
        $relationIndex += count(array_intersect($this->flavor, $that->flavor));
        $relationIndex += count(array_intersect($this->ideal_for, $that->ideal_for));
        $relationIndex += count(array_intersect($this->pairing, $that->pairing));
        $relationIndex -= abs($this->price - $that->price) * 0.01;
        $relationIndex -= abs($this->abv - $that->abv) * 0.1;
        return $relationIndex;
    }

    public function relatedProducts()
    {
        $relatedA = \DB::table('product_relations')
            ->addSelect('product_relations.correlation')
            ->where('product_relations.product_a', $this->id)
            ->join('products', 'products.id', 'product_relations.product_b')
            ->addSelect('products.id');
        $relatedB = \DB::table('product_relations')
            ->addSelect('product_relations.correlation')
            ->where('product_relations.product_b', $this->id)
            ->join('products', 'products.id', 'product_relations.product_a')
            ->addSelect('products.id');
        return $relatedA->union($relatedB)->orderBy('correlation', 'DESC');
    }

    public function copyFromRequest(\Illuminate\Http\Request $request)
    {
        $this->name = $request->name;
        $this->description = $request->description;
        $this->category_id = $request->category_id;
        $this->volume = $request->volume;
        $this->volume_units = $request->volume_units;
        $this->highlighted = $request->highlighted;
        $this->price = $request->price;
        $this->abv = $request->abv;
        $this->flavor = json_decode($request->flavor);
        $this->pairing = json_decode($request->pairing);
        $this->ideal_for = json_decode($request->ideal_for);
    }

    public function scopeHighlighted($query)
    {
        return $query->where('highlighted', 1);
    }

    public function getPriceAttribute()
    {
        return $this->attributes['price'] / 100;
    }

    public function getRawPriceAttribute()
    {
        return $this->attributes['price'];
    }

    public function setPriceAttribute($value)
    {
        $this->attributes['price'] = round($value * 100, 2);
    }
}
