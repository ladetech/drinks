<?php

use Illuminate\Http\Request;

Route::prefix('user')->middleware(['auth'])->group(function () {
    Route::get('/phone_numbers', 'UserController@getPhoneNumbers');
    Route::post('/phone_numbers', 'UserController@updatePhoneNumbers');
    Route::post('/logout', 'UserController@logout');
    Route::post('/', 'UserController@updateInfo');
    Route::patch('/password', 'UserController@changePassword');
    Route::delete('/', 'UserController@deleteAccount');
});

Route::post('/am-of-age', 'SessionController@confirmAge');
Route::post('/accept-cookies', 'SessionController@acceptCookies');

Route::get('/media', 'MediaController@list');
Route::post('/media', 'MediaController@create');
Route::patch('/media/{id}', 'MediaController@update');
Route::delete('/media/{id}', 'MediaController@delete');

Route::get('/addresses', 'AddressController@index');
Route::post('/address', 'AddressController@add');
Route::patch('/address/{id}', 'AddressController@edit');
Route::delete('/address/{id}', 'AddressController@remove');

Route::get('/orders/admin-pending', 'OrderController@indexAdminPending')->middleware('admin');
Route::get('/orders', 'OrderController@index');
Route::post('/order', 'OrderController@create');

Route::patch('/orders/{order_id}/mark-as-shipped', 'OrderController@markAsShipped');
Route::patch('/orders/{order_id}/shipment-status', 'OrderController@updateShipmentStatus');
Route::patch('/orders/{order_id}/mark-as-delivered', 'OrderController@markAsDelivered');

Route::get('/products', 'ProductController@index');
Route::get('/products/highlighted', 'ProductController@indexHighlighted');
Route::get('/product/search', 'ProductController@search');
Route::get('/product/{product_id}', 'ProductController@show');
Route::get('/product/{product_id}/related', 'ProductController@getRelated');
Route::get('/product/{product_id}/recipes', 'ProductController@getRecipes');
// TODO: make these exclusive to admin
Route::post('/product', 'ProductController@insert');
Route::patch('/product', 'ProductController@edit');
Route::delete('/products/{id}', 'ProductController@delete');

Route::get('/recipes', 'RecipeController@index');
Route::post('/recipe', 'RecipeController@insert');
Route::patch('/recipe', 'RecipeController@edit');
Route::delete('/recipe', 'RecipeController@delete');

Route::get('/stats', 'StatsController@summary');

Route::get('/stores', 'StoreController@index');
Route::post('/store', 'StoreController@insert');
Route::patch('/store', 'StoreController@edit');
Route::delete('/store', 'StoreController@delete');

Route::get('/categories', 'CategoryController@index');
Route::get('/categories/root', 'CategoryController@indexRoot');
Route::get('/categories/{category_id}/products', 'CategoryController@products');
Route::post('/category', 'CategoryController@save');
Route::delete('/category/{id}', 'CategoryController@delete');

Route::group(['middleware' => ['web']], function () {
    Route::get('/cart', 'CartController@show');
    Route::post('/cart', 'CartController@add');
    Route::patch('/cart', 'CartController@update');
    Route::delete('/cart/{id}', 'CartController@remove');
});

// stripe webhooks
Route::post('/stripe-hooks', 'StripeController@handleWebhook');

// facebook
Route::get('/facebook/callback', 'FacebookController@handleCallback');
