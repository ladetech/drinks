<?php

Route::get('/', 'HomeController@index');

Route::get('/privacidad', 'HomeController@privacy');

Route::get('/productos', 'ProductController@showAll');
Route::get('/producto/{slug}', 'ProductController@showInfo');

Route::get('/recetas/{slug}', 'RecipeController@showRecipe');

Route::get('/carrito', 'CartController@index')->name('cart.show');

Route::middleware(['auth'])->group(function () {
    Route::get('/orden/preparar', 'OrderController@prepareCheckout')->name('order.preparation');
    Route::get('/orden/{order_id}/pago', 'OrderController@redirectToCheckout')->name('order.checkout');
    Route::get('/orden/{order_id}/estado', 'OrderController@showStatus')->name('order.status');
    Route::get('/orden/exito/{session_id}', 'OrderController@success')->name('order.success');
});

Route::get('/login/facebook', 'FacebookController@redirect');

Auth::routes();

Route::prefix('admin')->group(function () {
    Route::get('/', 'AdminController@showDashboard');
    Route::get('/medios', 'AdminController@showMediaDashboard');
    Route::get('/productos', 'AdminController@showProductDashboard');
    Route::get('/recetas', 'AdminController@showRecipeDashboard');
    Route::get('/sucursales', 'AdminController@showStoreDashboard');
    Route::get('/ofertas', 'AdminController@showOfferDashboard');
    Route::get('/ordenes', 'AdminController@showOrderDashboard');
});

// user routes
Route::prefix('usuario')->middleware(['auth'])->group(function () {
    Route::get('/', 'UserController@showDashboard')->name('user.dashboard');
    Route::get('/ordenes', 'UserController@showOrders')->name('user.orders');
    Route::get('/direcciones', 'UserController@showAddresses')->name('user.addresses');
    Route::get('/configuracion', 'UserController@showConfig')->name('user.config');
});

Route::get('/admin/post-deployment', 'DeploymentController@postDeployment');
