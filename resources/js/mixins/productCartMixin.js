export default {
    data() {
        return {
            loading: false,
            id: this.product ? this.product.id : ''
        }
    },
    computed: {
        quantityInCart() {
            let item = { id: this.id, type: 'product' }
            return this.$store.getters.quantityInCart(item)
        },
    },
    methods: {
        addToCart(quantity) {
            let t = this
            let item = {
                type: 'product',
                id: this.id,
                quantity: quantity
            };

            t.loading = true
            this.$store.dispatch('addToCart', item).then(() => {
                t.loading = false
            })
        },
        removeFromCart() {
            let t = this
            let item = {
                type: 'product',
                id: this.id,
            };

            t.loading = true

            this.$store.dispatch('removeFromCart', item).then(() => {
                t.loading = false
            })
        },
        updateCartQuantity(quantity) {
            let t = this;
            console.log(this.id)
            let item = {
                type: 'product',
                id: this.id,
                quantity: quantity,
            };
            t.loading = true;
            this.$store.dispatch('updateCartQuantity', item).then(() => {
                t.loading = false
            })
        }
    }
};
