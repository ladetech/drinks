import Vue from 'vue'

import RelatedProducts from './components/RelatedProducts.vue'
import ProductShow from './components/ProductShow.vue'

new Vue({
    el: '#productShow',
    components: { ProductShow, RelatedProducts }
})
