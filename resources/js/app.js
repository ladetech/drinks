require('./bootstrap')

import Vue from 'vue'
import Vuex, { mapGetters } from 'vuex'

import 'es6-promise'

import VueCurrencyFilter from 'vue-currency-filter'
Vue.use(VueCurrencyFilter, {
    symbol : '$',
    thousandsSeparator: ',',
    fractionCount: 2,
    fractionSeparator: '.',
    symbolPosition: 'front',
    symbolSpacing: true
})

import Simplebar from 'simplebar-vue'
Vue.component('simplebar', Simplebar)

import Homepage from './components/views/Homepage.vue'
import Cart from './components/views/Cart.vue'
import ProductsAll from './components/views/ProductsAll.vue'
import ProductShowPage from './components/views/ProductShowPage.vue'
import RecipePage from './components/views/RecipePage.vue'
import OrderSetup from './components/views/order/OrderSetup.vue'
import OrderStatus from './components/views/order/OrderStatus.vue'

Vue.use(Vuex)
import addressModule from './modules/addresses'
import cartModule from './modules/cart'
import categoryModule from './modules/categories'
import productModule from './modules/products'
import orderModule from './modules/orders'
import storeModule from './modules/stores'
import userModule from './modules/user'

import VueFlashMessage from 'vue-flash-message'
Vue.use(VueFlashMessage)

const store = new Vuex.Store({
    modules: {
        cartModule,
        categoryModule,
        productModule,
        addressModule,
        orderModule,
        storeModule,
        userModule
    },
})

const app = new Vue({
    el: 'main',
    store,
    data: {
        showNavigation: false,
        cart: []
    },
    components: { Cart, Homepage, ProductsAll, ProductShowPage, RecipePage, OrderSetup, OrderStatus },
    computed: {
        ...mapGetters({
            cartCount: 'countCartItems',
            categories: 'getCategories',
        })
    },
    methods: {
        toggleNavigation() {
            this.showNavigation = !this.showNavigation
        },
        acceptCookies() {
            axios.post('accept-cookies')
        }
    },
    created() {
        store.dispatch('fetchCart')
        store.dispatch('fetchRootCategories')
    }
})
