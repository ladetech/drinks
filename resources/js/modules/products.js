import Vue from 'vue'

const productModule = {
    state: {
        products: [],
        productSearchResults: []
    },
    mutations: {
        loadProducts(state, payload) {
            state.products = payload
        },
        loadProductSearchResults(state, payload) {
            state.productSearchResults = payload
        },
        loadProductRecipes(state, payload) {
            let index = _.findIndex(state.products, { id: payload.productId })
            if (index == -1) {
                state.products.push({
                    id: payload.productId
                })
                index = state.products.length - 1
            }
            Vue.set(state.products[index], 'recipes', payload.data)
        },
        loadRelatedProducts(state, payload) {
            let index = _.findIndex(state.products, { id: payload.productId })
            if (index == -1) {
                state.products.push({
                    id: payload.productId
                })
                index = state.products.length - 1
            }
            Vue.set(state.products[index], 'related', payload.data)
        },
    },
    actions: {
        fetchRelatedProducts({ commit }, productId) {
            return axios.get('/product/' + productId + '/related').then((r) => {
                commit('loadRelatedProducts', {
                    productId: productId,
                    data: r.data
                })
                return r
            }).catch((e) => {
                alert('Error cargando productos relacionados')
                return e
            })
        },
        fetchProductRecipes({ commit }, productId) {
            return axios.get('/product/' + productId + '/recipes').then((r) => {
                commit('loadProductRecipes', {
                    productId: productId,
                    data: r.data
                })
                return r
            }).catch((e) => {
                alert('Error cargando recetas de producto')
                return e
            })
        },
        fetchProducts({ commit }, params) {
            return axios.get('products', {
                params: params
            }).then((r) => {
                commit('loadProducts', r.data)
                commit('loadProductSearchResults', r.data)
                return r
            }).catch((e) => {
                alert('Error cargando productos')
                return e
            })
        },
        fetchHighlightedProducts({ commit }, params) {
            return axios.get('products/highlighted', {
                params: params
            }).then((r) => {
                commit('loadProducts', r.data)
                commit('loadProductSearchResults', r.data)
                return r
            }).catch((e) => {
                alert('Error cargando productos')
                return r
            })
        },
        searchProducts({ commit }, payload) {
            return axios.get('product/search', {
                params: payload
            }).then((r) => {
                commit('loadProductSearchResults', r.data)
                return r
            }).catch((e) => {
                alert('Error cargando resultados de búsqueda')
                return e
            })
        },
    },
    getters: {
        getProducts(state) {
            return state.products
        },
        getHighlightedProducts(state) {
            return _.map(state.products, product => product.highlighted)
        },
        getProductFlavors(state) {
            return _.uniq(_.flatten(state.products.map((p) => { return p.flavor })))
        },
        getProductIdeals(state) {
            return _.uniq(_.flatten(state.products.map((p) => { return p.ideal_for })))
        },
        getProductPairings(state) {
            return _.uniq(_.flatten(state.products.map((p) => { return p.pairing })))
        },
        getProductSearchResults(state) {
            return state.productSearchResults
        },
        getProductTypes(state) {
            return _.uniq(state.products.map((p) => { return p.type }))
        },
        getRelatedProducts: (state) => (productId) => {
            let index = _.findIndex(state.products, { id: productId })
            if (index == -1) {
                state.products.push({
                    id: productId
                })
                index = state.products.length - 1
            }
            return state.products[index].related
        },
        getProductRecipes: (state) => (productId) => {
            let index = _.findIndex(state.products, { id: productId })
            if (index == -1) {
                state.products.push({
                    id: productId
                })
                index = state.products.length - 1
            }
            return state.products[index].recipes
        },
    },
}

export default productModule
