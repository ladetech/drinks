import Vue from 'vue'

const defaultFlashOptions = {
    timeout: 3000
}

const cartModule = {
    state() {
        return {
            cart: [],
        }
    },
    mutations: {
        addToCart(state, payload) {
            // add only if item is not already in cart
            if (_.findIndex(state.cart, { item: { id: payload.id } }) == -1)
            state.cart.push({ item: { id: payload.id }, quantity: payload.quantity })
        },
        loadCart(state, cart) { state.cart = cart },
        updateCartQuantity(state, payload) {
            let idx = _.findIndex(state.cart, { item: { id: payload.id } })
            state.cart[idx].quantity = payload.quantity;
        },
        removeFromCart(state, payload) {
            let idx = _.findIndex(state.cart, { item: { id: payload.id } })
            if (idx != -1)
            state.cart.splice(idx, 1)
        },
    },
    actions: {
        addToCart({ commit }, item) {
            return axios.post('cart', {
                'type': item.type,
                'id': item.id,
                'quantity': item.quantity,
            }).then(function (r) {
                commit('addToCart', { id: item.id, quantity: item.quantity })
                Vue.prototype.$flashStorage.flash('Articulo añadido al carrito', 'info', defaultFlashOptions)
            }).catch(function (e) {
                Vue.prototype.$flashStorage.flash('Error añadiendo artículo al carrito', 'warning', defaultFlashOptions)
            })
        },
        fetchCart({ commit }) {
            return axios.get('cart').then(function (r) {
                commit('loadCart', r.data)
            }).catch(function (e) {
                console.log(e)
                Vue.prototype.$flashStorage.flash('Error cargando el carrito', 'danger', defaultFlashOptions)
            })
        },
        updateCartQuantity({ commit }, item) {
            return axios.patch('cart', { id: item.id, quantity: item.quantity }).then(() => {
                commit('updateCartQuantity', item)
                Vue.prototype.$flashStorage.flash('Se ha cambiado la cantidad de artículos en el carrito', 'info', defaultFlashOptions)
            })
        },
        removeFromCart({ commit }, item) {
            return axios.delete('cart/' + item.id).then((r) => {
                commit('removeFromCart', item)
                Vue.prototype.$flashStorage.flash('Artículo quitado del carrito', 'info', defaultFlashOptions)
            })
        },
    },
    getters: {
        getCart(state) {
            return state.cart
        },
        countCartItems(state) { return state.cart.length },
        quantityInCart: (state) => (item) => {
            let idx = _.findIndex(state.cart, { item: { id: item.id } })

            if (-1 == idx)
            return 0
            return state.cart[idx].quantity
        },
    },
}

export default cartModule
