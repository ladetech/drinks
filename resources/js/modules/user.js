const userModule = {
    state() {
        return {
            user: {
                name: '',
                email: '',
                phone_numbers: []
            }
        }
    },
    actions: {
        fetchUserPhoneNumbers({ commit }) {
            return axios.get('user/phone_numbers').then((r) => {
                commit('setPhoneNumbers', r.data)
            })
        },
        updatePhoneNumbers({ commit }, payload) {
            let data = new FormData()
            console.log("updatePhoneNumbers")
            data.append('phone_numbers', JSON.stringify(payload))

            return axios.post('user/phone_numbers', data).then((r) => {
                commit('setPhoneNumbers', r.data)
            })
        }
    },
    mutations: {
        setPhoneNumbers(state, payload) {
            state.user.phone_numbers = payload
        }
    },
    getters: {
        getUser(state) {
            return state.user
        }
    }
}

export default userModule
