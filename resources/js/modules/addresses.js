import Vue from 'vue'

const defaultFlashOptions = {
    timeout: 3000
}

const addressModule = {
    state() {
        return {
            addresses: []
        }
    },
    mutations: {
        deleteAddress(state, payload) {
            let idx = _.findIndex(state.addresses, { 'id': payload.id })
            state.addresses.splice(idx, 1)
        },
        editAddress(state, payload) {
            let idx = _.findIndex(state.addresses, { 'id': payload.id })
            Object.assign(state.addresses[idx], payload)
        },
        insertAddress(state, payload) {
            state.addresses.push(payload)
        },
        loadAddresses(state, payload) {
            state.addresses = payload
        },
    },
    actions: {
        deleteAddress({ commit }, address) {
            return axios.delete('address/' + address.id).then((r) => {
                commit('deleteAddress', address)
                Vue.prototype.$flashStorage.flash('Dirección de envío eliminada', 'info', defaultFlashOptions)
                return r
            }).catch((e) => {
                Vue.prototype.$flashStorage.flash('Error eliminando dirección de entrega', 'warning', defaultFlashOptions)
                return e
            })
        },
        editAddress({ commit }, payload) {
            let data = new FormData()

            for (let prop in payload.address)
                data.append(prop, payload.address[prop])

            data.append('_method', 'PATCH')

            return axios.post('address/' + payload.address.id, data).then((r) => {
                commit('editAddress', r.data)
                Vue.prototype.$flashStorage.flash('Dirección de entrega editada', 'info', defaultFlashOptions)
                return r
            }).catch((e) => {
                Vue.prototype.$flashStorage.flash('No fue posible editar la dirección de entrega', 'warning', defaultFlashOptions)
                return e
            })
        },
        fetchAddresses({ commit }) {
            return axios.get('addresses').then((r) => {
                commit('loadAddresses', r.data);
            }).catch((e) => {
                Vue.prototype.$flashStorage.flash('No fue posible cargar las direcciones de entrega', 'danger', defaultFlashOptions)
            })
        },
        insertAddress({ commit }, payload) {
            let data = new FormData()

            for (let prop in payload.address)
                data.append(prop, payload.address[prop])

            return axios.post('address', data).then((r) => {
                commit('insertAddress', r.data)
                Vue.prototype.$flashStorage.flash('Dirección de entrega añadida', 'info', defaultFlashOptions)
                return r
            }).catch((e) => {
                Vue.prototype.$flashStorage.flash('No fue posible agregar dirección de entrega', 'warning', defaultFlashOptions)
                return e
            })
        },
    },
    getters: {
        getAddresses(state) {
            return state.addresses
        },
    }
}

export default addressModule
