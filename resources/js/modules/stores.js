import Vue from 'vue'

const defaultFlashOptions = {
    timeout: 3000
}

const storeModule = {
    state: {
        stores: []
    },
    mutations: {
        loadStores(state, payload) {
            state.stores = payload
        },
    },
    actions: {
        fetchStores({ commit }) {
            return axios.get('stores').then((r) => {
                commit('loadStores', r.data)
            }).catch(() => {
                Vue.prototype.$flashStorage.flash("Error cargando sucursales", 'danger', defaultFlashOptions)
            })
        },
    },
    getters: {
        getStores(state) {
            return state.stores
        }
    },
}

export default storeModule
