import Vue from 'vue'

const defaultFlashOptions = {
    timeout: 3000
}

const orderModule = {
    state: {
        orders: []
    },
    mutations: {
        loadOrders(state, payload) {
            state.orders = payload
        },
    },
    actions: {
        fetchOrders({ commit }, options) {
            let params = options
            axios.get('orders', { params: params }).then((r) => {
                if (!r.isAxiosError)
                    commit('loadOrders', r.data)
            }).catch((e) => {
                Vue.prototype.$flashStorage.flash('Dirección de envío eliminada', 'info', defaultFlashOptions)
            })
        },
    },
    getters: {
        getOrders(state) {
            return state.orders
        },
        getPaymentPendingOrders(state) {
            return state.orders.filter((o) => {
                return !o.paid
            })
        },
        getShippingPendingOrders(state) {
            return state.orders.filter((o) => {
                return o.paid && !o.shipped
            })
        },
        getFulfilledOrders(state) {
            return state.orders.filter((o) => {
                return o.paid && o.shipped
            })
        },
    }
}

export default orderModule
