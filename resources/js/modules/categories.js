import Vue from 'vue'

const defaultFlashOptions = {
    timeout: 3000
}

const categoryModule = {
    state() {
        return {
            categories: []
        }
    },
    mutations: {
        loadCategories(state, categories) {
            state.categories = categories
        },
    },
    actions: {
        fetchRootCategories({ commit, state }) {
            if (state.categories.length != 0)
                return;

            axios.get('categories/root').then((response) => {
                commit('loadCategories', response.data)
            }).catch((error) => {
                console.log(error)
                Vue.$flashStorage.flash("Error cargando categorías", 'danger')
            })
        },
        fetchCategories({ commit }) {
            axios.get('categories').then((response) => {
                commit('loadCategories', response.data)
            }).catch((error) => {
                console.log(error)
                Vue.$flashStorage.flash("Error cargando categorías", 'danger')
            })
        },
    },
    getters: {
        getCategories(state) {
            return state.categories
        },
        getParentCategoryById: (state, getters) => (categoryId) => {
            return getters.getRootCategoryById(getters.getCategoryById(categoryId).parent_id)
        },
        getCategoryById(state, categoryId) {
            let result = null;

            state.categories.some(function iterate(category) {
                if (category.id === categoryId) {
                    result = category
                    return true
                }

                return Array.isArray(category.subcategories) && category.subcategories.some(iterate)
            })

            return result
        },
        getSubcategories: (state, getters) => (categoryId) => {
            return getters.getCategoryById(categoryId).subcategories
        },
        getRootCategoryById: (state, getters) => (categoryId) => {
            let category = _.cloneDeep(getters.getCategoryById(categoryId))

            while (category.parent_id)
                category = _.cloneDeep(getters.getCategoryById(category.parent_id))

            return category
        },
        getRootCategory: (state, getters) => (category) => {
            while (category.parent_id)
                category = _.cloneDeep(getters.getCategoryById(category.parent_id))

            return category
        },
    }
}

export default categoryModule
