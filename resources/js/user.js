require('./bootstrap')

import Vue from 'vue'
import Vuex from 'vuex'

import UserDashboard from './components/user/UserDashboard'
import UserAddressDashboard from './components/user/AddressDashboard'
import UserOrderDashboard from './components/user/OrderDashboard'
import UserConfigDashboard from './components/user/ConfigDashboard'

Vue.use(Vuex)
import addressModule from './modules/addresses'
import orderModule from './modules/orders'
import cartModule from './modules/cart'

import VueFlashMessage from 'vue-flash-message'
Vue.use(VueFlashMessage)

const store = new Vuex.Store({
    modules: { addressModule, orderModule, cartModule }
})

const app = new Vue({
    el: 'main',
    store,
    components: { UserDashboard, UserAddressDashboard, UserOrderDashboard, UserConfigDashboard },
})
