require('./bootstrap')

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
    $(document).on("wheel", "input[type=number]", function (e) {
            $(this).blur();

    });
})

const defaultFlashOptions = {
    timeout: 3000
}

import Vue from 'vue'
import Vuex, { mapGetters } from 'vuex'

import AdminDashboard from './components/admin/views/Dashboard'
import OrderDashboard from './components/admin/views/OrderDashboard'
import MediaDashboard from './components/admin/views/MediaDashboard'
import ProductDashboard from './components/admin/views/ProductDashboard'
import RecipeDashboard from './components/admin/views/RecipeDashboard'
import StoreDashboard from './components/admin/views/StoreDashboard'

Vue.use(Vuex)

import VueCurrencyFilter from 'vue-currency-filter'
Vue.use(VueCurrencyFilter, {
    symbol : '$',
    thousandsSeparator: ',',
    fractionCount: 2,
    fractionSeparator: '.',
    symbolPosition: 'front',
    symbolSpacing: true
})

import VueFlashMessage from 'vue-flash-message'
Vue.use(VueFlashMessage)

const store = new Vuex.Store({
    state: {
        orders: [],
        products: [],
        productSearchResults: [],
        recipes: [],
        stores: [],
        stats: {},
        categories: []
    },
    actions: {
        // CATEGORIES
        deleteCategory({ commit }, categoryId) {
            axios.delete('category/' + categoryId).then((response) => {
                commit('deleteCategory', categoryId)
                Vue.prototype.$flashStorage.flash("Se ha eliminado una categoría", 'info', defaultFlashOptions)
            }).catch((error) => {
                console.log(error)
                Vue.prototype.$flashStorage.flash("No fue posible eliminar esta categoría", 'warning', defaultFlashOptions)
            })
        },
        insertCategory({ commit }, category) {
            let data = new FormData()

            for (let property in category)
                data.append(property, category[property])

            return axios.post('category', data).then((response) => {
                commit('insertCategory', response.data)
                Vue.prototype.$flashStorage.flash("Se ha agregado una categoría", 'info', defaultFlashOptions)
                return response.data
            }).catch((error) => {
                console.log(error)
                Vue.prototype.$flashStorage.flash("Error agregando categoría", 'danger', defaultFlashOptions)
            })
        },
        fetchCategories({ commit }) {
            axios.get('categories').then((response) => {
                commit('loadCategories', response.data)
            }).catch((error) => {
                console.log(error)
                Vue.prototype.$flashStorage.flash("Error cargando categorías", 'danger')
            })
        },
        // ORDERS
        fetchOrders({ commit }) {
            return axios.get('orders/admin-pending').then((r) => {
                commit('loadOrders', r.data)
            }).catch((e) => {
                Vue.prototype.$flashStorage.flash('Error cargando órdenes pendientes', 'danger', defaultFlashOptions)
            })
        },
        markOrderAsShipped({ commit }, orderId) {
            return axios.patch('orders/' + orderId + "/mark-as-shipped").then((r) => {
                commit('markOrderAsShipped', orderId)
                Vue.prototype.$flashStorage.flash('Orden marcada como enviada', 'info', defaultFlashOptions)
            }).catch((e) => {
                Vue.prototype.$flashStorage.flash('Error al marcar la orden como enviada', 'warning', defaultFlashOptions)
            })
        },
        updateOrderShipmentStatus({ commit }, orderId) {
            return axios.patch('orders/' + orderId + "/shipment-status").then((r) => {
                commit('updateOrderShipmentStatus', orderId)
                Vue.prototype.$flashStorage.flash('Se ha actualizado el estado de la orden', 'info', defaultFlashOptions)
            }).catch((e) => {
                Vue.prototype.$flashStorage.flash('Error actualizando estado de la orden', 'warning', defaultFlashOptions)
            })
        },
        markOrderAsDelivered({ commit }, orderId) {
            return axios.patch('orders/' + orderId + "/mark-as-delivered").then((r) => {
                commit('markOrderAsDelivered', orderId)
                Vue.prototype.$flashStorage.flash('La orden se ha marcado como entregada', 'info', defaultFlashOptions)
            }).catch((e) => {
                Vue.prototype.$flashStorage.flash('Error marcando orden como entregada', 'warning', defaultFlashOptions)
            })
        },
        // PRODUCTS
        // delete product
        deleteProduct({ commit }, id) {
            return axios.delete(`products/${id}`).then((r) => {
                commit('deleteProduct', id)
                Vue.prototype.$flashStorage.flash('Producto eliminado', 'info', defaultFlashOptions)
            }).catch((e) => {
                console.log(e)
                Vue.prototype.$flashStorage.flash('Error eliminando el producto', 'warning', defaultFlashOptions)
            })
        },
        editProduct({ commit }, payload) {
            let data = new FormData()

            for (let prop in payload.product) {
                if (Array.isArray(payload.product[prop]))
                    data.append(prop, JSON.stringify(payload.product[prop]))
                else
                    data.append(prop, payload.product[prop])
            }

            data.append('_method', 'PATCH')

            return axios.post('product', data , { 'Content-Type': "x-www-form-urlencoded" }).then((r) => {
                let product = r.data
                product.category_id = Number(product.category_id)
                commit('editProduct', { slug: payload.slug, product: product })
                Vue.prototype.$flashStorage.flash('Producto editado', 'info', defaultFlashOptions)
            }).catch((e) => {
                console.log(e)
                Vue.prototype.$flashStorage.flash('Error editando producto', 'warning', defaultFlashOptions)
            })
        },
        fetchProducts({ commit }) {
            axios.get('products', {
                params: { params: ['products.*'] }
            }).then((r) => {
                commit('loadProducts', r.data)
                commit('loadProductSearchResults', r.data)
            }).catch((e) => {
                Vue.prototype.$flashStorage.flash('Error cargando productos', 'danger', defaultFlashOptions)
            })
        },
        insertProduct({ commit }, product) {
            let data = new FormData()

            for (let prop in product) {
                if (Array.isArray(product[prop]))
                    data.append(prop, JSON.stringify(product[prop]))
                else
                    data.append(prop, product[prop])
            }

            return axios.post('product', data, { 'Content-Type': "x-www-form-urlencoded" }).then((r) => {
                let product = r.data
                product.category_id = Number(product.category_id)

                commit('insertProduct', { newProduct: product })
                Vue.prototype.$flashStorage.flash('Producto guardado', 'success', defaultFlashOptions)
            }).catch((e) => {
                console.log(e)
                Vue.prototype.$flashStorage.flash('Error guardando producto', 'warning', defaultFlashOptions)
            })
        },
        searchProducts({ commit }, payload) {
            axios.get('product/search', {
                params: payload
            }).then((r) => {
                commit('loadProductSearchResults', r.data)
            }).catch((e) => {
                Vue.prototype.$flashStorage.flash('Error cargando resultados de la búsqueda', 'danger', defaultFlashOptions)
            })
        },
        // RECIPES
        deleteRecipe({ commit }, recipe) {
            return axios.delete('recipe', { params: { slug: recipe.slug } }).then((r) => {
                commit('deleteRecipe', { slug: recipe.slug })
                Vue.prototype.$flashStorage.flash('Receta eliminada', 'info', defaultFlashOptions)
            }).catch((e) => {
                Vue.prototype.$flashStorage.flash('Error eliminando receta', 'warning', defaultFlashOptions)
            })
        },
        editRecipe({ commit }, payload) {
            let data = new FormData()

            for (let prop in payload.recipe)
                data.append(prop, payload.recipe[prop])

            data.append('_method', 'PATCH')

            return axios.post('recipe', data , { 'Content-Type': "x-www-form-urlencoded" }).then((r) => {
                commit('editRecipe', { slug: payload.slug, recipe: r.data.recipe })
                Vue.prototype.$flashStorage.flash('Receta editada', 'info', defaultFlashOptions)
            }).catch((e) => {
                console.log(e)
                Vue.prototype.$flashStorage.flash('Error editando receta', 'warning', defaultFlashOptions)
            })
        },
        fetchRecipes({ commit }) {
            axios.get('recipes').then((r) => {
                commit('loadRecipes', r.data)
            }).catch((e) => {
                Vue.prototype.$flashStorage.flash('Error cargando recetas', 'danger', defaultFlashOptions)
            })
        },
        insertRecipe({ commit }, recipe) {
            let data = new FormData()

            for (let prop in recipe)
                data.append(prop, recipe[prop])

            return axios.post('recipe', data, { 'Content-Type': "x-www-form-urlencoded" }).then((r) => {
                commit('insertRecipe', { newRecipe: r.data.recipe })
                Vue.prototype.$flashStorage.flash('Receta guardada', 'success', defaultFlashOptions)
            }).catch((e) => {
                console.log(e)
                Vue.prototype.$flashStorage.flash('Error guardando receta', 'warning', defaultFlashOptions)
            })
        },
        // STATS
        fetchStats({ commit }, payload) {
            return axios.get('stats', {
                params: payload
            }).then((r) => {
                commit('loadStats', r.data)
            }).catch((e) => {
                Vue.prototype.$flashStorage.flash('Error cargando estadísticas', 'warning', defaultFlashOptions)
                return e
            })
        },
        // STORES
        deleteStore({ commit }, store) {
            return axios.delete('store', { params: { id: store.id } }).then((r) => {
                commit('deleteStore', { id: store.id })
                Vue.prototype.$flashStorage.flash('Sucursal eliminada', 'info', defaultFlashOptions)
                return r
            }).catch((e) => {
                Vue.prototype.$flashStorage.flash('Error eliminando sucursal', 'warning', defaultFlashOptions)
                return e
            });
        },
        editStore({ commit }, payload) {
            let data = new FormData();

            for (let key in payload.store)
                data.append(key, payload.store[key])

            data.append('_method', 'PATCH')

            return axios.post('store', data).then((r) => {
                commit('editStore', { id: payload.store.id, store: r.data.store })
                Vue.prototype.$flashStorage.flash('Sucursal editada', 'info', defaultFlashOptions)
                return r
            }).catch((e) => {
                console.log(e)
                Vue.prototype.$flashStorage.flash('Error editando sucursal', 'warning', defaultFlashOptions)
                return e
            })
        },
        fetchStores({ commit }) {
            return axios.get('stores').then((r) => {
                commit('loadStores', r.data)
            }).catch(() => {
                Vue.prototype.$flashStorage.flash('Error cargando sucursales', 'danger', defaultFlashOptions)
            })
        },
        insertStore({ commit }, store) {
            let data = new FormData()

            for (let key in store)
                data.append(key, store[key])

            return axios.post('store', data).then((r) => {
                commit('insertStore', { newStore: store })
                Vue.prototype.$flashStorage.flash('Sucursal guardada', 'success', defaultFlashOptions)
                return r
            }).catch((e) => {
                Vue.prototype.$flashStorage.flash('Error guardando sucursal', 'warning', defaultFlashOptions)
                return e
            })
        },
    },
    mutations: {
        // CATEGORIES
        deleteCategory(state, categoryId) {
            state.categories.some(function iterate(category) {
                if (!category.hasOwnProperty('subcategories'))
                    return false;

                const isCategoryParent = category.subcategories.some(c => c.id == categoryId)

                if (isCategoryParent) {
                    const index = _.findIndex(category.subcategories, { id: categoryId })
                    category.subcategories.splice(index, 1)
                    return true
                }

                return Array.isArray(category.subcategories) && category.subcategories.some(iterate)
            })
        },
        insertCategory(state, newCategory) {
            if (!newCategory.hasOwnProperty('subcategories'))
                newCategory.subcategories = []

            if (!newCategory.parent_id) {
                state.categories.push(newCategory)
                return
            }

            state.categories.some(function iterate(category) {
                if (category.id == newCategory.parent_id) {
                    category.subcategories.push(newCategory)
                    return true
                }

                return Array.isArray(category.subcategories) && category.subcategories.some(iterate)
            })
        },
        loadCategories(state, categories) {
            state.categories = categories
        },
        // ORDERS
        markOrderAsShipped(state, orderId) {
            state.orders[_.findIndex(state.orders, { 'id': orderId })].shipment_status = 'Enviado'
        },
        updateOrderShipmentStatus(state, payload) {
            state.orders[_.findIndex(state.orders, { 'id': payload.orderId })].shipment_status = payload.shipment_status
        },
        markOrderAsDelivered(state, orderId) {
            let idx = _.findIndex(state.orders, { 'id': orderId })
            state.orders[idx].shipment_status = 'Entregado'
            state.orders[idx].delivered = true
        },
        loadOrders(state, payload) {
            state.orders = payload
        },
        // PRODUCTS
        deleteProduct(state, id) {
            let idx = _.findIndex(state.productSearchResults, { 'id': id });
            state.productSearchResults.splice(idx, 1);
        },
        editProduct(state, payload) {
            let idx = _.findIndex(state.products, { 'slug': payload.slug })
            Object.assign(state.products[idx], payload.product)
        },
        insertProduct(state, payload) {
            state.products.push(_.cloneDeep(payload.newProduct))
        },
        loadProducts(state, payload) {
            state.products = payload
        },
        loadProductSearchResults(state, payload) {
            state.productSearchResults = payload
        },
        // RECIPES
        deleteRecipe(state, payload) {
            let idx = _.findIndex(state.recipes, { 'slug': payload.slug })
            state.recipes.splice(idx, 1)
        },
        editRecipe(state, payload) {
            let idx = _.findIndex(state.recipes, { 'slug': payload.slug })
            state.recipes[idx] = payload.recipe
        },
        insertRecipe(state, payload) {
            state.recipes.push(payload.newRecipe)
        },
        loadRecipes(state, payload) {
            state.recipes = payload
        },
        // STORES
        deleteStore(state, payload) {
            let idx = _.findIndex(state.stores, { 'id': payload.id })
            state.stores.splice(idx, 1)
        },
        editStore(state, payload) {
            let idx = _.findIndex(state.stores, { 'id': payload.id })
            state.stores[idx] = payload.store
        },
        insertStore(state, payload) {
            state.stores.push(payload.newStore)
        },
        loadStats(state, payload) {
            Vue.set(state, 'stats', payload)
        },
        loadStores(state, payload) {
            state.stores = payload
        },
    },
    getters: {
        // CATEGORIES
        getCategories(state) {
            return state.categories
        },
        getParentCategoryById(state, getters) {
            return (categoryId) => {
                return getters.getRootCategoryById(getters.getCategoryById(categoryId).parent_id)
            }
        },
        getCategoryById(state) {
            return (categoryId) => {
                let result = null;

                state.categories.some(function iterate(category) {
                    if (category.id === categoryId) {
                        result = category
                        return true
                    }

                    return Array.isArray(category.subcategories) && category.subcategories.some(iterate)
                })

                return result
            }
        },
        getSubcategories(state, getters) {
            return (categoryId) => {
                return getters.getCategoryById(categoryId).subcategories
            }
        },
        getRootCategoryById(state) {
            return (categoryId) => {
                let category = _.cloneDeep(this.getCategoryById(categoryId))

                while (category.parent_id)
                    category = _.cloneDeep(this.getCategoryById(category.parent_id))

                return category
            }
        },
        getRootCategory(state) {
            return (category) => {
                while (category.parent_id)
                    category = _.cloneDeep(this.getCategoryById(category.parent_id))

                return category
            }
        },
        // ORDERS
        countPendingOrders(state) {
            return state.orders.length
        },
        getPendingShipmentOrders(state) {
            return state.orders.filter(o => !o.shipment_status)
        },
        getShippedOrders(state) {
            return state.orders.filter(o => o.shipment_status && !o.delivered)
        },
        getConflictedOrders(state) {
            return state.orders.filter(o => o.conflict)
        },
        // PRODUCTS
        getProducts(state) {
            return state.products
        },
        getProductFlavors(state) {
            return _.uniq(_.flatten(state.products.map((p) => { return p.flavor })))
        },
        getProductIdeals(state) {
            return _.uniq(_.flatten(state.products.map((p) => { return p.ideal_for })))
        },
        getProductPairings(state) {
            return _.uniq(_.flatten(state.products.map((p) => { return p.pairing })))
        },
        getProductSearchResults(state) {
            return state.productSearchResults
        },
        getProductTypes(state) {
            return _.uniq(state.products.map((p) => { return p.type }))
        },
        // RECIPES
        getRecipes(state) {
            return state.recipes
        },
        // STATS
        getStats(state) {
            return state.stats
        },
        // STORES
        getStores(state) {
            return state.stores
        }
    },
})

const app = new Vue({
    el: 'main',
    store,
    data: {  },
    components: { MediaDashboard, OrderDashboard, ProductDashboard, RecipeDashboard, StoreDashboard, AdminDashboard },
    computed: {
        ...mapGetters({ pendingOrders: 'countPendingOrders' })
    },
    created() {
        this.$store.dispatch('fetchOrders')
    }
})
