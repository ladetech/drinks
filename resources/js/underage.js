require('./bootstrap')

import Vue from 'vue'

new Vue({
    el: 'main',
    methods: {
        goBack() {
            window.history.back()
        },
        confirmAge() {
            axios.post('am-of-age').then(() => {
                window.location.reload()
            })
        }
    }
})
