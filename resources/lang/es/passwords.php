<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Las contraseñas deben de tener al menos ocho caracteres y coincidir con la confirmación.',
    'reset' => 'Su contraseña ha sido reestablecida',
    'sent' => 'Le hemos mandado el enlace de reestablecimiento de contraseña por correo electrónico.',
    'token' => 'Este token de reestablecimiento de contraseña es inválido.',
    'user' => "No encontramos ningún usuario con esa dirección de correo electrónico.",

];
