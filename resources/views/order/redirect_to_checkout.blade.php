@extends('layouts.app')

@section('title', "Redirigiendo a página de pago")

@section('app')
    <div class="container mt-5">
        <h1>Redirigiendo a la página de pago</h1>
    </div>
@endsection

@section('javascripts')
    <script src="https://js.stripe.com/v3/"></script>
    <script>
    var stripe = Stripe( "{{ config('services.stripe.key') }}" );
    stripe.redirectToCheckout({
        sessionId: '{{ $order->stripe_session_id }}'
    }).then(function (result) {
        alert("Error redirigiendo a la página de pago. Inténtelo de nuevo más tarde")
    })
</script>
@endsection
