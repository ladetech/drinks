@extends('layouts.app')

@section('title', 'Estado de la orden')

@section('app')
    <order-status :order='@json($order)'></order-status>
@endsection
