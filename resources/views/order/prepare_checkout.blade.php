@extends('layouts.app')

@section('title', "Preparar compra")

@section('app')
    <div class="container mt-5">
        <h2>¡Ya casi terminas!</h2>
        <order-setup></order-setup>
    </div>
@endsection
