<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Plataforma bajo construcción. Plataforma de servicio de bebidas alcohólicas a domicilio en Tepatitlán de Morelos, Jalisco">
    <meta name="robots" content="noarchive"/>
    <title>Drinks Zone: Plataforma bajo construcción</title>
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}"/>
</head>
<body>
    <main class="background-dots">
        <div class="container">
            <div class="row pt-5">
                <div class="offset-lg-2 col-lg-3 col-md-6">
                    <img src="{{ asset('/storage/marketing/logo-dark.png') }}" alt="Drinks Zone" class="img-fluid">
                </div>
                <div class="col-lg-5 col-md-6 pt-5">
                    <h1>Viene algo bueno</h1>
                    <p>¡Pronto podrás recibir tus bebidas de Drinks Zone en tus puertas!</p>
                </div>
            </div>
        </div>
    </main>
</body>
</html>
