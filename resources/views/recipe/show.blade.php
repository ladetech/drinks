@extends('layouts.app')

@section('title', $recipe->name)

@section('app')
    <div class="container my-5">
        <div class="row">
            <div class="col-md-8">
                <recipe-page :recipe='@json($recipe)'>
                    <template slot="title">
                        {{ $recipe->name }}
                    </template>
                    <template slot="description">
                        {{ $recipe->description }}
                    </template>
                    <template slot="steps">
                        {{ $recipe->steps }}
                    </template>
                </recipe-page>
            </div>
            <div class="col-md-4">
            </div>
        </div>
    </div>
@endsection
