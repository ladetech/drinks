@extends('layouts.app')
@section('title', "Inicio")

@section('stylesheets')
    <link rel="stylesheet" href="{{ asset('css/home.css') }}">
@endsection

@section('app')
    <homepage></homepage>
@endsection
