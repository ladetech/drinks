<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    @yield('meta')
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
</head>
<body>
    <nav class="navbar-light bg-light d-md-none">
        <div class="container">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#userDashboardNavigationBar">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div id="userDashboardNavigationBar" class="collapse navbar-collapse">
                <ul class="navbar-nav">
                    <li class="nav-item"><a class="nav-link" href="/">Volver</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('user.dashboard') }}">Mi cuenta</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('user.addresses') }}">Direcciones</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('user.orders') }}">Órdenes</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('user.config') }}">Configuración</a></li>
                    <li class="nav-item"><a class="nav-link logout-button" href="#">Salir</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <aside class="d-none d-md-block col-md-3">
                <ul class="nav flex-md-column sticky-top py-md-5">
                    <li class="nav-item"><a class="nav-link" href="/">Volver</a></li>
                    <hr>
                    <li class="nav-item"><a class="nav-link" href="{{ route('user.dashboard') }}">Mi cuenta</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('user.addresses') }}">Direcciones</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('user.orders') }}">Órdenes</a></li>
                    <hr>
                    <li class="nav-item"><a class="nav-link" href="{{ route('user.config') }}">Configuración</a></li>
                    <hr>
                    <li class="nav-item"><a class="nav-link logout-button" href="#">Salir</a></li>
                </ul>
            </aside>
            <section class="col-md-9 py-5">
                <h1>@yield('title')</h1>
                <p class="text-muted">@yield('description')</p>
                <main>
                    @yield('app')
                    <flash-message></flash-message>
                </main>
            </section>
        </div>
    </div>
    <script src="{{ asset('/js/user.js') }}"></script>
</body>
</html>
