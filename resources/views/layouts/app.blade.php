<!DOCTYPE html>
<html lang="es">
<head>
    @yield('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta charset="UTF-8">
    <title>@yield('title') | Drinks Zone</title>
    <link rel="favicon" href="/favicon.ico">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    @yield('stylesheets')
</head>
<body>
    <main>
        <noscript>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 offset-md-2 mt-5">
                        <div class="alert alert-danger">
                            <b>Error: </b>
                            Esta página necesita que javascript esté activado para funcionar correctamente.
                        </div>
                    </div>
                </div>
            </div>
        </noscript>
        @if(session('is-over-18') || Cookie::get('is-over-18'))
            @if(!session('accepted-cookies') && Cookie::get('accepted-cookies') !== 'true')
                @component('components.cookies_disclaimer')
                @endcomponent
            @endif
            @component('components.navigation')
            @endcomponent
            @yield('content')
            @yield('app')
            <template v-if="cartCount">
                <a class="cart-shortcut" href="/carrito">
                    <i class="icon link__icon icon--cart">
                        <span class="cart-contents">@{{ cartCount }}</span>
                    </i>
                </a>
            </template>
            <flash-message></flash-message>
        @else
            @component('components.underage_disclaimer')
            @endcomponent
        @endif
    </main>
    <script type="text/javascript">var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();(function(){var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];s1.async=true;s1.src='https://embed.tawk.to/5fea667edf060f156a9159ac/1eqlq0nia';s1.charset='UTF-8';s1.setAttribute('crossorigin','*');s0.parentNode.insertBefore(s1,s0); })();</script>
    @if(session('is-over-18'))
        <script src="/js/manifest.js"></script>
        <script src="/js/vendor.js"></script>
        <script src="/js/app.js"></script>
        @yield('javascripts')
        @component('components.footer')
        @endcomponent
    @else
        <script src="/js/manifest.js"></script>
        <script src="/js/vendor.js"></script>
        <script src="{{ asset('js/underage.js') }}"></script>
    @endif
</body>
</html>
