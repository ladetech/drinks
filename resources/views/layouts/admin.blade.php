<!DOCTYPE html>
<html lang="es">
<head>
	@yield('meta')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta charset="UTF-8">
	<title>@yield('title')</title>
	<link rel="stylesheet" href="{{ asset('css/app.css') }}">
	@yield('stylesheets')
</head>
<body>
    <main>
        <noscript>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 offset-md-2 mt-5">
                        <div class="alert alert-danger">
                            <b>Error: </b>
                            Esta página necesita que javascript esté activado para funcionar correctamente.
                        </div>
                    </div>
                </div>
            </div>
        </noscript>
        <div class="container-fluid">
            <div class="row">
                <aside class="col-xl-2 col-md-3">
                    <ul class="nav flex-md-column sticky-top py-md-5">
                        <li class="nav-item">
                            <a class="h5 nav-link" href="/admin">Administrador</a>
                        </li>
                        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Agregar, editar y eliminar productos">
                            <a class="nav-link" href="/admin/productos">Productos</a>
                        </li>
                        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Agregar, editar y eliminar recetas">
                            <a class="nav-link" href="/admin/recetas">Recetas</a>
                        </li>
                        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Agregar, editar y eliminar medios / imágenes">
                            <a class="nav-link" href="/admin/medios">Medios</a>
                        </li>
                        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Administrar órdenes">
                            <a class="nav-link" href="/admin/ordenes">
                                Órdenes
                                <span class="badge badge-warning">@{{ pendingOrders }}</span>
                            </a>
                        </li>
                        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Agregar, editar y eliminar sucursales">
                            <a class="nav-link" href="/admin/sucursales">Sucursales</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/">Volver al inicio</a>
                        </li>
                    </ul>
                </aside>
                <section class="col-xl-10 col-md-9">
                    <div class="container py-5">
                        <h2>@yield('title')</h2>
                        <small class="text-muted">@yield('description')</small>
                        @yield('app')
                    </div>
                </section>
            </div>
        </div>
        <flash-message></flash-message>
    </main>
    <script src="/js/manifest.js"></script>
    <script src="/js/vendor.js"></script>
	<script src="{{ asset('js/admin.js') }}"></script>
    @yield('javascripts')
	@component('components.footer')
	@endcomponent
</body>
