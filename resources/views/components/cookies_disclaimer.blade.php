<div class="cookies-disclaimer alert alert-primary fixed-bottom mb-0">
    <h5>Usamos cookies</h5>
    <p>Las cookies son piezas de información usadas para darte un mejor servicio. Al seguir navegando en este sitio das a entender que estás de acuerdo con el uso de las cookies.</p>
    <button class="btn btn-primary" @click="acceptCookies" data-dismiss="alert" type="button">Estoy de acuerdo</button>
</div>
