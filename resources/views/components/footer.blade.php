<footer>
    <section id="badges" class="bg-light py-3">
        <div class="container text-right">
            <img src="{{ asset('/storage/vendor/3_Card_color_horizontal.svg') }}" class="img-fluid" style="max-height: 2em">
            <a href="//stripe.com" target="_blank">
                <img src="{{ asset('/storage/vendor/powered_by_stripe.svg') }}" class="img-fluid">
            </a>
        </div>
    </section>
    <section id="footerNavigation" class="bg-light text-dark py-5">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <ul class="list-unstyled m-0">
                        <li><a href="/">Inicio</a></li>
                        <li><a href="/productos">Productos</a></li>
                        <li><a href="/privacidad">Privacidad</a></li>
                    </ul>
                </div>
                <div class="col-sm-4">
                    <p style="font-size: 0.5em;">Una plataforma desarrollada por <a href="https://antoniojai.me">Ladetech</a></p>
                </div>
            </div>
        </div>
    </section>
    <section id="footerCopyright" class="bg-dark text-center text-light py-3">
        <h6>Copyright © <a target="_blank" href="//alzaterra.com.mx">Alza Terra</a>. Todos los derechos reservados</h6>
    </section>
</footer>
