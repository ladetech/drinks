<nav class="navigation" :class="{ 'visible': showNavigation }" :tabindex="showNavigation - 1" @focus="showNavigation = true">
	<button class="navigation-toggle" @click="toggleNavigation" role="button"><span class="sr-only">Mostrar navegación</span></button>
    <ul class="navigation__links nav flex-column flex-nowrap">
        <li class="nav-item">
            <a class="nav-link" href="/"><i class="icon link__icon icon--home"></i> Inicio</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/productos"><i class="icon link__icon icon--products"></i> Productos</a>
            <ul class="nav flex-column flex-nowrap">
                <li class="nav-item category-nav-item" v-for="category in categories">
                    <a class="nav-link" :href="'/productos?c=' + category.id">@{{ category.name }}</a>
                </li>
            </ul>
        </li>
        <li class="nav-item cart">
            <a class="nav-link" href="/carrito">
                <i class="icon link__icon icon--cart">
                    <span class="cart-contents">@{{ cartCount }}</span>
                </i>
                Mi carrito
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/#shops"><i class="icon link__icon icon--stores"></i> Sucursales</a>
        </li>
        <li class="nav-item">&nbsp;</li>
        @if(Auth::user())
            <li class="nav-item">
                <a class="nav-link" href="{{ route('user.dashboard') }}"><i class="icon link__icon icon--user"></i> Mi cuenta</a>
            </li>
            <li class="nav-item">
                <a class="nav-link logout-button" href="#"><i class="icon link__icon icon--logout"></i> Cerrar sesión</a>
            </li>

            @if(Auth::user()->role == 'admin')
                <li class="nav-item">
                    <a class="nav-link" href="/admin"><i class="icon link__icon icon--admin"></i> Administrador</a>
                </li>
            @endif
        @else
            <li class="nav-item">
                <a class="nav-link" href="/login"><i class="icon link__icon icon--user"></i> Iniciar sesión</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/register"><i class="icon link__icon icon--logout"></i> Crear una cuenta</a>
            </li>
        @endif
    </ul>
</nav>
