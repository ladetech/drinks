<div class="container">
    <div class="card bg-secondary text-light my-5">
        <div class="card-header">
            <h5 class="card-title">Confirma tu mayoría de edad</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-4 text-center mb-5">
                    <svg width="150" height="150" version="1.1" viewBox="0 0 39.687 39.687" xmlns="http://www.w3.org/2000/svg">
                        <g transform="matrix(3.5345 0 0 3.5345 -3.5345 -3.5345)" style="fill:#fff;stroke-width:.28293">
                            <path d="m6.6152 1c-3.0959 0-5.6152 2.5193-5.6152 5.6152s2.5193 5.6133 5.6152 5.6133 5.6133-2.5174 5.6133-5.6133-2.5174-5.6152-5.6133-5.6152zm0 0.79492c2.667 0 4.8203 2.1534 4.8203 4.8203s-2.1534 4.8203-4.8203 4.8203-4.8203-2.1534-4.8203-4.8203 2.1534-4.8203 4.8203-4.8203z" style="opacity:.5;fill:#fff"/>
                            <g style="fill:#fff;stroke-width:.28293">
                                <path d="m3.7581 5.0455v0.18909c0 0.21161-0.12156 0.30165-0.33767 0.30165h-0.41871v0.42771h0.73386v2.2196h0.54027v-3.1381z" style="fill:#fff"/>
                                <path d="m6.4367 8.2286c0.6078 0 1.1616-0.32416 1.1616-0.97248 0-0.34217-0.15308-0.61681-0.45473-0.77889 0.18459-0.13056 0.30165-0.32416 0.30165-0.5943 0-0.55377-0.48624-0.88244-1.0085-0.88244-0.52226 0-1.0085 0.32866-1.0085 0.88244 0 0.27013 0.11706 0.46373 0.30165 0.5943-0.30165 0.16208-0.45473 0.43672-0.45473 0.77889 0 0.65282 0.55828 0.97248 1.1616 0.97248zm0-1.9044c-0.27914 0-0.46823-0.15308-0.46823-0.4052 0-0.24762 0.1936-0.4007 0.46823-0.4007s0.46823 0.15308 0.46823 0.4007c0 0.25213-0.18909 0.4052-0.46823 0.4052zm0 1.3912c-0.35117 0-0.62131-0.17108-0.62131-0.50875 0-0.32866 0.25663-0.51325 0.62131-0.51325s0.62131 0.18459 0.62131 0.51325c0 0.33767-0.27013 0.50875-0.62131 0.50875z" style="fill:#fff"/>
                                <path d="m8.8541 7.9449h0.45022v-0.92746h0.92296v-0.45022h-0.92296v-0.92296h-0.45022v0.92296h-0.92746v0.45022h0.92746z" style="fill:#fff"/>
                            </g>
                        </g>
                    </svg>
                </div>
                <div class="col-md-8">
                    <p class="text-justify">Necesitas confirmar que eres mayor de edad para continuar en este sitio</p>
                    <div class="row">
                        <div class="col-lg-6">
                            <a class="btn btn-outline-light btn-block mb-3" href="http://alcoholinformate.org.mx/">Soy menor</a>
                        </div>
                        <div class="col-lg-6">
                            <button class="btn btn-outline-primary btn-block" @click="confirmAge">Soy mayor de 18 años</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
