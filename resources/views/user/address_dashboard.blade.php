@extends('layouts.user_dashboard')

@section('title', "Mis direcciones")

@section('description', "Aquí puedes agregar, cambiar o eliminar direcciones de envío")

@section('app')
    <user-address-dashboard></user-address-dashboard>
@endsection
