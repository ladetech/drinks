@extends('layouts.user_dashboard')

@section('title', "Mi cuenta")

@section('app')
    <user-dashboard :user='@json($user)'></user-dashboard>
@endsection
