@extends('layouts.user_dashboard')

@section('title', "Configuración de mi cuenta")

@section('app')
    <user-config-dashboard :user-model='@json($user)'></user-config-dashboard>
@endsection
