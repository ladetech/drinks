@extends('layouts.app')

@section('title', 'Carrito')

@section('stylesheets')
    <link rel="stylesheet" href="{{ asset('css/cart.css') }}">
@endsection

@section('app')
    <cart></cart>
@endsection
