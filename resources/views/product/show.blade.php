@extends('layouts.app')
@section('title', "$product->name $product->volume$product->volume_units")
@section('stylesheets')
    <link rel="stylesheet" href="{{ asset('css/product_show.css') }}">
@endsection
@section('app')
    <product-show-page :product='{{ $product->toJson() }}'></product-show-page>
@endsection
