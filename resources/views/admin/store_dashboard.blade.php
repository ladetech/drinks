@extends('layouts.admin')
@section('title', 'Administrador de sucursales')
@section('description', 'Aquí puede administrar las sucursales que se muestran en el mapa.')
@section('app')
    <store-dashboard></store-dashboard>
@endsection
