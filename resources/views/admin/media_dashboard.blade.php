@extends('layouts.admin')
@section('title', 'Administrador de medios')
@section('description', 'Aquí puede administrar los medios que se muestran a través de su página.')
@section('app')
    <media-dashboard></media-dashboard>
@endsection
