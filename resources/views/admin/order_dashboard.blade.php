@extends('layouts.admin')
@section('title', 'Administrador de órdenes')
@section('description', 'Aquí puede observar y administrar las órdenes que se han hecho con la plataforma.')

@section('app')
    <order-dashboard></order-dashboard>
@endsection
