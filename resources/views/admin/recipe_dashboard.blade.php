@extends('layouts.admin')
@section('title', "Administrador de recetas")
@section('description', "Aquí podrá administrar las recetas para cocteles que usen sus productos")
@section('app')
    <recipe-dashboard></recipe-dashboard>
@endsection
