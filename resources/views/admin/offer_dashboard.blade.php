@extends('layouts.admin')
@section('title', 'Administrador de ofertas')
@section('description', 'Aquí puede administrar las ofertas de la plataforma, así como las condiciones para que cada una se cumpla.')
