@extends('layouts.admin')
@section('title', 'Administrador de productos')
@section('description', 'Aquí puede administrar los productos que se manejan, destacar productos y consultar información de popularidad de los productos.')
@section('app')
    <product-dashboard></product-dashboard>
@endsection
