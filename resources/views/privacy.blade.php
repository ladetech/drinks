@extends('layouts.app')
@section('title', "Privacidad")

@section('app')
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <h1>Protegemos tu información</h1>
                <h2>Información personal</h2>
                <p>
                La información que tú, el usuario, provees a nosotros
                (Red Comercial Drinks Zone)
                al momento de registrar una cuenta, iniciar sesión a través de
                un servicio de terceros o modificar tus datos se almacena de
                manera segura y se mantiene dentro de la red comercial en todo
                momento.
                </p>
                <h3>Información de compras</h3>
                <p>
                La información sobre tus órdenes como los productos ordenados,
                la dirección de envío y los teléfonoes de contacto se mantiene
                dentro de la red comercial en todo momento y solo se utilizan
                para darte el mejor servicio posible.
                </p>
                <h3>Información de pago</h3>
                <p>
                Los pagos por medio de tarjeta de crédito son procesados por un
                servicio de terceros,
                <a href="https://stripe.com" target="_blank">Stripe</a>.
                Puedes consultar su política de privacidad siguiendo
                <a href="https://stripe.com/privacy">este enlace</a>.
                </p>
            </div>
        </div>
    </div>
@endsection
