<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Order;
use App\Product;
use App\User;
use App\Address;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Order::class, function (Faker $faker) {
    $paid = $faker->boolean();
    $delivered = $paid && $faker->boolean();

    return [
        'stripe_session_id' => Str::random(60),
        'amount' => 0,
        'paid' => $paid,
        'delivered' => $delivered,
        'conflict' => 0,
        'shipment_status' => 'Entregado',
    ];
});

$factory->afterMaking(Order::class, function ($order, Faker $faker) {

    $nbProducts = rand(1, 5);
    $products = [];
    $orderProducts = [];

    for ($i=0; $i<$nbProducts; $i++) {
        $products[$i]['type'] = 'product';
        do {
            $products[$i]['id'] = Product::pluck('slug')->random();
        } while (in_array($products[$i]['id'], $orderProducts));
        array_push($orderProducts, $products[$i]['id']);
        $products[$i]['quantity'] = rand(1, 10);
    }

    $order->products = $products;

    $order->user_id = User::pluck('id')->random();
    $order->address_id = Address::pluck('id')->random();
});
