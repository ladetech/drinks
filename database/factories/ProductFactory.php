<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'slug' => $faker->slug(),
        'name' => $faker->sentence($nbWords = 3, $variableNbWords = true),
        'image_path' => $faker->image('storage/app/public/products/', 300, 800, 'nightlife', false),
        'abv' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 100),
        'description' => $faker->text($nbChars=200),
        'flavor' => $faker->words($nb = 2, $asText = false),
        'pairing' => $faker->words($nb = 2, $asText = false),
        'ideal_for' => $faker->words($nb = 2, $asText = false),
        'price' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 2000),
        'highlighted' => $faker->boolean(),
        'volume' =>  $faker->numberBetween($min=1, $max=1000),
        'volume_units' => ['ml', 'lt', 'kg'][array_rand(['ml', 'lt', 'kg'])],
        'category_id' => NULL,
    ];
});
