<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('products', function (Blueprint $table) {
      // ID-ing
      $table->increments('id');
      $table->string('slug')->unique();
      $table->string('name');
      // classification
      $table->string('type');
      // about
      $table->text('description');
      $table->float('abv');
      $table->float('volume');
      $table->string('volume_units', 4);
      // reccomended
      $table->json('pairing');
      $table->json('ideal_for');
      $table->json('flavor');
      // selling
      $table->float('price', 8, 2);
      $table->boolean('highlighted')->default(false);
      // image
      $table->string('image_path')->nullable();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('products');
    Schema::dropIfExists('product_product');
  }
}
